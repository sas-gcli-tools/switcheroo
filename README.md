# switcheroo

[![Image](https://gitlab.com/sas-gcli-tools/switcheroo/-/badges/release.svg)](https://gitlab.com/sas-gcli-tools/switcheroo/-/releases) [![pipeline status](https://gitlab.com/sas-gcli-tools/switcheroo/badges/main/pipeline.svg)](https://gitlab.com/sas-gcli-tools/switcheroo/-/commits/main) [![Image](https://www.vipm.io/package/sas_workshops_lib_switcheroo_for_g_cli/badge.svg?metric=installs)](https://www.vipm.io/package/sas_workshops_lib_switcheroo_for_g_cli/) [![Image](https://www.vipm.io/package/sas_workshops_lib_switcheroo_for_g_cli/badge.svg?metric=stars)](https://www.vipm.io/package/sas_workshops_lib_switcheroo_for_g_cli/)
<!-- start short_description -->
A G-CLI tool for switching out source libraries to test PPLs and VIPM packages.
<!-- end short_description -->

[TOC]

## Description
<!-- start description -->
A G-CLI tool for switching source libraries to PPLs and installed VIPM packages for testing. Allows you to write tests once for the source and then reuse those tests against the built/installed library.

<!-- end description -->

## Getting Started

- Get the latest release
    - on [VIPM](https://www.vipm.io/package/sas_workshops_lib_switcheroo_for_g_cli/). 
    - on our [relases page](https://gitlab.com/sas-gcli-tools/switcheroo/-/releases)
- See the [Example](#example) section to get started.

## Options
<!-- start options -->
This G-CLI Tool has the following options:

flag | parameters | required | description
--- | --- | --- | ---
--test-dir, -t | _test directory_ | yes | Directory of Tests to load - these are the callers of the library you are going to replace.
--original, -o | _original library_ | yes | Library to be replaced 
--replacement, -r | _replacement library_ | yes | Original Library will be replaced with this.

**NOTE:** for original and replacement you can use `<vilib>` which will get replaced with the path to vilib.
<!-- end options -->

## Example

<!-- start example -->
```bash
# G-CLI options ignored for simplicity.
# -- seperates the g-cli options from the options for this specific tool.
g-cli switcheroo -- -t "tests" -o "mylib.lvlib" -r "mylib.lvlibp"
or
g-cli switcheroo -- -t "tests" -o "mylib.lvib" -r "<vilib>/myrenamedlib.lvlib"
```
<!-- end example -->
 
 ## Other G-CLI tools
 
 You can find other SAS G-CLI Tools [here](https://sas-gcli-tools.gitlab.io)

