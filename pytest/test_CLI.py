import subprocess
from os import environ
from approvaltests import verify, Options
from approvaltests.scrubbers import create_regex_scrubber
import pytest
import shlex
import re

TOOL = "switheroo"


@pytest.fixture()
def tool_cmd():
    return ["g-cli", TOOL, "--"]


@pytest.fixture()
def run_tool(tool_cmd):
    def run(options_str):
        return subprocess.run(
            tool_cmd + shlex.split(options_str), capture_output=True, text=True
        )

    return run


def test_verify_no_params(run_tool):
    output = run_tool("")
    scrubber = create_regex_scrubber(r"Elapsed Time: .+\\n", "Elapsed Time: x.x")
    verify(output, options=Options().with_scrubber(scrubber))


# def test_hash(run_tool):
#     tool_hash = run_tool("--hash")
#     git_output = subprocess.run(
#         ["git", "rev-parse", "HEAD"], capture_output=True, text=True
#     )
#     assert tool_hash.stdout.strip() == git_output.stdout.strip()


# def test_version(run_tool):
#     tool_version = run_tool("--version")
#     tool_version.stdout.strip() == environ[
#         "FULL_VERSION"
#     ]  # this is run as a post-install test script. That script should set this based on vars.yml and the build number.
